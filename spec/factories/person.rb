FactoryBot.define do
  factory :person do
    first_name { Faker::Name.first_name }
    middle_name { Faker::Name.middle_name }
    last_name { Faker::Name.last_name }
    nickname { Faker::Name.unique.first_name }
    birthdate { Faker::Date.birthday }
    insurance_id { Faker::IDNumber.invalid }
    notes { Faker::Lorem.paragraphs.join(' ') }
  end
end
