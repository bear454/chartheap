FactoryBot.define do
  factory :visit do
    visited_at { Faker::Time.between(5.days.ago, 5.days.from_now) }
    association :person
    association :provider
    notes { Faker::Lorem.paragraphs.join(' ') }
  end
end
