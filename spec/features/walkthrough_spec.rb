require 'rails_helper'

RSpec.feature "walkthrough", type: :feature, js: true do
  let(:person_template) { build(:person) }
  let(:provider_template) { build(:provider) }
  let(:visit_template) { build(:visit, person: nil, provider: nil) }

  scenario "Add a Person, edit them. Add a Provider, edit them. Add a visit, edit it. Delete everything." do
    # Person
    visit "/"
    click_link 'Add a new person'

    fill_in 'First name', with: person_template.first_name
    fill_in 'Middle name', with: person_template.middle_name
    fill_in 'Last name', with: person_template.last_name
    fill_in 'Nickname', with: person_template.nickname
    select person_template.birthdate.year, from: 'person[birthdate(1i)]'
    select I18n.t("date.month_names")[person_template.birthdate.month], from: 'person[birthdate(2i)]'
    select person_template.birthdate.day, from: 'person[birthdate(3i)]'
    fill_in 'Insurance', with: person_template.insurance_id
    fill_in 'Notes', with: person_template.notes
    click_button 'Save'

    expect(page).to have_content('saved')
    expect(page).to have_content(person_template.nickname)
    person = Person.find_by_nickname(person_template.nickname)
    expect(person.first_name).to eq(person_template.first_name)
    expect(person.middle_name).to eq(person_template.middle_name)
    expect(person.last_name).to eq(person_template.last_name)
    expect(person.insurance_id).to eq(person_template.insurance_id)
    expect(person.notes).to eq(person_template.notes)

    click_link 'Edit'
    fill_in 'Notes', with: ''
    click_button 'Save'

    expect(page).to have_content('updated')
    expect(page).to have_content(person_template.nickname)
    person.reload
    expect(person.notes).to be_blank

    # Provider
    visit "/"
    click_link 'Add a new provider'

    fill_in 'Name', with: provider_template.name
    fill_in 'Address', with: provider_template.address
    fill_in 'Phone', with: provider_template.phone
    fill_in 'Primary contact', with: provider_template.primary_contact
    fill_in 'Notes', with: provider_template.notes
    click_button 'Save'

    expect(page).to have_content('saved')
    expect(page).to have_content(provider_template.primary_contact)
    provider = Provider.find_by_primary_contact(provider_template.primary_contact)
    expect(provider.name).to eq(provider_template.name)
    expect(provider.address).to eq(provider_template.address)
    expect(provider.phone).to eq(provider_template.phone)
    expect(provider.notes).to eq(provider_template.notes)

    click_link 'Edit'
    fill_in 'Notes', with: ''
    click_button 'Save'

    expect(page).to have_content('updated')
    expect(page).to have_content(provider_template.primary_contact)
    provider.reload
    expect(provider.notes).to be_blank

    # Visit

    visit "/"
    click_link 'Add a new visit'

    select visit_template.visited_at.year, from: 'visit[visited_at(1i)]'
    select I18n.t("date.month_names")[visit_template.visited_at.month], from: 'visit[visited_at(2i)]'
    select visit_template.visited_at.day, from: 'visit[visited_at(3i)]'
    select visit_template.visited_at.strftime('%H'), from: 'visit[visited_at(4i)]'
    select visit_template.visited_at.strftime('%M'), from: 'visit[visited_at(5i)]'
    find_field('visit[person_id]').find("option[value='#{person.id}']").select_option
    find_field('visit[provider_id]').find("option[value='#{provider.id}']").select_option
    fill_in 'Notes', with: visit_template.notes
    click_button 'Save'

    expect(page).to have_content('saved')
    expect(page).to have_content(visit_template.visited_at.localtime.strftime('%Y %B %d - %H:%M'))
    visit = Visit.find_by_person_id_and_provider_id(person.id, provider.id)
    expect(visit.person_id).to eq(person.id)
    expect(visit.provider_id).to eq(provider.id)
    expect(visit.notes).to eq(visit_template.notes)

    click_link 'Edit'
    fill_in 'Notes', with: ''
    click_button 'Save'

    expect(page).to have_content('updated')
    expect(page).to have_content(visit.visited_at.localtime.strftime('%Y %B %d - %H:%M'))
    visit.reload
    expect(visit.notes).to be_blank

    # deletions

    # Visit
    visit visit_path(visit)
    page.accept_confirm do
      click_link 'Delete'
    end

    expect(page).to have_content('deleted')
    expect{ Visit.find(visit.id) }.to raise_error(ActiveRecord::RecordNotFound)

    # Provider
    visit provider_path(provider)
    page.accept_confirm do
      click_link 'Delete'
    end

    expect(page).to have_content('deleted')
    expect{ Provider.find(provider.id) }.to raise_error(ActiveRecord::RecordNotFound)

    # Person
    visit person_path(person)
    page.accept_confirm do
      click_link 'Delete'
    end

    expect(page).to have_content('deleted')
    expect{ Person.find(person.id) }.to raise_error(ActiveRecord::RecordNotFound)
  end
end
