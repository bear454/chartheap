require 'rails_helper'

RSpec.feature "homepage", type: :feature, js: true do
  let!(:people) { 10.times.collect{ create(:person) } }
  let!(:providers) { 10.times.collect{ create(:provider) } }

  before do
    visit '/'
  end

  it 'shows the most recently updated people' do
    limit = WelcomeController::PEOPLE_TILE_LIMIT
    Person.order(updated_at: :desc).limit(limit).each do |person|
      expect(page).to have_content(person)
    end
  end

  it 'links to more people' do
    within '#people' do
      expect(page).to have_link(href: people_path)
    end
  end

  it 'shows the most recently updated providers' do
    limit = WelcomeController::PROVIDERS_TILE_LIMIT
    Provider.order(updated_at: :desc).limit(limit).each do |provider|
      expect(page).to have_content(provider.primary_contact)
      expect(page).to have_content(provider.name)
    end
  end

  it 'links to more providers' do
    within '#providers' do
      expect(page).to have_link(href: providers_path)
    end
  end

  it 'shows the most recently occurring visits' do
    limit = WelcomeController::VISITS_TILE_LIMIT
    Visit.order(visited_at: :desc).limit(limit).each do |visit|
      expect(page).to have_link(href: visit_path(visit))
    end
  end

  it 'links to more visits' do
    within '#visits' do
      expect(page).to have_link(href: visits_path)
    end
  end
end
