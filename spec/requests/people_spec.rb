require 'rails_helper'

RSpec.describe "People", type: :request do
  describe "GET /people" do
    let!(:people) { 2.times.collect { create(:person) } }

    before do
      get people_path
    end

    it "works!" do
      expect(response).to have_http_status(200)
    end

    it "lists people" do
      people.each do |person|
        expect(response.body).to include(person.nickname)
      end
    end
  end
end
