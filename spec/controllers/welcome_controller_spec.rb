require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  it 'should have a welcome view' do
    get :index
    expect(response).to render_template('index')
  end
end
