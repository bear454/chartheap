require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:flash_keys) { %w(notice success error alert) }

  describe '#flash_class' do
    it 'has a string value for each flash key' do
      flash_keys.each do |flash_key|
        expect(helper.flash_class[flash_key]).to be_a(String)
      end
    end
  end

  describe '#flash_icon' do
    it 'has a string value for each flash key' do
      flash_keys.each do |flash_key|
        expect(helper.flash_icon[flash_key]).to be_a(String)
      end
    end
  end
end
