require 'rails_helper'

RSpec.describe Person, type: :model do
  let(:person) { create(:person) }

  it 'has a unique nickname' do
    expect{ Person.create!(nickname: person.nickname) }.to raise_error{ ActiveRecord::RecordInvalid }
  end

  it 'has a string represenation that matches uniqueness' do
    expect(person.to_s).to eq(person.nickname)
  end
end
