require 'rails_helper'

RSpec.describe Provider, type: :model do
  let(:provider) { create(:provider) }

  it 'has a unique combination of primary contact and name' do
    expect{ Provider.create!(name: provider.name, primary_contact: provider.primary_contact) }.to raise_error(ActiveRecord::RecordInvalid)
  end

  it 'has a string represenation that matches uniqueness' do
    expect(provider.to_s).to eq("#{provider.primary_contact} at #{provider.name}")
  end
end
