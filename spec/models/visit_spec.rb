require 'rails_helper'

RSpec.describe Visit, type: :model do
  context 'on create' do
    let(:person) { create(:person) }
    let(:provider) { create(:provider) }

    it 'touches the person and provider' do
      person_last_updated_at = person.updated_at
      provider_last_updated_at = provider.updated_at

      create(:visit, person: person, provider: provider)
      person.reload
      provider.reload

      expect(person.updated_at).to be > person_last_updated_at
      expect(provider.updated_at).to be > provider_last_updated_at
    end
  end

  context 'by default' do
    let!(:visits) { 10.times.collect{ create(:visit) } }

    it 'orders by visited_at' do
      expect(Visit.first.id).to eq(Visit.order(visited_at: :desc).first.id)
    end
  end
end
