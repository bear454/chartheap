Rails.application.routes.draw do
  resources :visits
  resources :providers
  resources :people

  get '/about', to: 'welcome#about', as: :about
  root 'welcome#index'
end
