json.extract! provider, :id, :name, :address, :phone, :primary_contact, :notes, :created_at, :updated_at
json.url provider_url(provider, format: :json)
