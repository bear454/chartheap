json.extract! visit, :id, :visited_at, :person_id, :provider_id, :notes, :created_at, :updated_at
json.url visit_url(visit, format: :json)
