json.extract! person, :id, :first_name, :middle_name, :last_name, :nickname, :birthdate, :insurance_id, :notes, :created_at, :updated_at
json.url person_url(person, format: :json)
