class WelcomeController < ApplicationController
  PEOPLE_TILE_LIMIT = 5
  PROVIDERS_TILE_LIMIT = 3
  VISITS_TILE_LIMIT = 7

  def index
    @people = Person.order(updated_at: :desc).limit(PEOPLE_TILE_LIMIT)
    @more_people = (Person.count > PEOPLE_TILE_LIMIT)

    @providers = Provider.order(updated_at: :desc).limit(PROVIDERS_TILE_LIMIT)
    @more_providers = (Provider.count > PROVIDERS_TILE_LIMIT)

    @visits = Visit.limit(VISITS_TILE_LIMIT)
    @more_visits = (Visit.count > VISITS_TILE_LIMIT)
  end

  def about; end
end
