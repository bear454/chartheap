module ApplicationHelper
  def navigation_links
    {
      'Home'      => root_path,
      'People'    => people_path,
      'Providers' => providers_path,
      'Visits'    => visits_path
    }
  end

  def flash_class
    {
      'notice'  => 'alert-info',
      'success' => 'alert-success',
      'error'   => 'alert-danger',
      'alert'   => 'alert-warning'
    }
  end

  def flash_icon
    {
      'notice'  => 'info-circle',
      'success' => 'check-circle',
      'error'   => 'exclamation-circle',
      'alert'   => 'exclamation-triangle'
    }
  end
end
