class Provider < ApplicationRecord
  validates_presence_of :name, :primary_contact
  validates_uniqueness_of :primary_contact, scope: :name
  validates_uniqueness_of :name, scope: :primary_contact

  def to_s
    "#{primary_contact} at #{name}"
  end
end
