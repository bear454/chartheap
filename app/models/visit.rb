class Visit < ApplicationRecord
  belongs_to :person, touch: true
  belongs_to :provider, touch: true
  has_many_attached :charts

  validates_presence_of :visited_at

  default_scope { order(visited_at: :desc) }
end
