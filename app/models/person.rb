class Person < ApplicationRecord
  has_one_attached :photo

  validates_presence_of :nickname
  validates_uniqueness_of :nickname

  def to_s
    nickname
  end

  def full_name
    [ first_name, middle_name, last_name ].compact.join(' ')
  end
end
