class CreateVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :visits do |t|
      t.datetime :visited_at
      t.references :person, foreign_key: true
      t.references :provider, foreign_key: true
      t.string :notes

      t.timestamps
    end
  end
end
