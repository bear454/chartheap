class CreateProviders < ActiveRecord::Migration[5.2]
  def change
    create_table :providers do |t|
      t.string :name
      t.text :address
      t.string :phone
      t.string :primary_contact
      t.text :notes

      t.timestamps
    end
  end
end
